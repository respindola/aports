# Contributor: Hygna <hygna@proton.me>
# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=pnpm
pkgver=8.12.1
pkgrel=0
pkgdesc="Fast, disk space efficient package manager"
url="https://pnpm.io"
arch="noarch"
license="MIT"
depends="nodejs"
source="https://registry.npmjs.org/pnpm/-/pnpm-$pkgver.tgz"
options="!check" # not implemented
builddir="$srcdir/package"

prepare() {
	default_prepare

	# remove node-gyp
	rm -rf dist/node-gyp-bin dist/node_modules/node-gyp
	# remove windows files
	rm -rf dist/vendor/*.exe

	# remove other unnecessary files
	find . -type f \( \
		-name '.*' -o \
		-name '*.cmd' -o \
		-name '*.bat' -o \
		-name '*.map' -o \
		-name '*.md' -o \
		-name '*.darwin*' -o \
		-name '*.win*' -o \
		-iname 'README*' \) -delete
}

package() {
	local DESTDIR="$pkgdir"/usr/share/node_modules/pnpm

	mkdir -p "$DESTDIR"
	cp -R "$builddir"/* "$DESTDIR"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sf ../share/node_modules/pnpm/bin/pnpm.cjs "$pkgdir"/usr/bin/pnpm
	ln -sf ../share/node_modules/pnpm/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx
}

sha512sums="
6705ea1966adb6587053e6cfe6fddc377f81066228daf736c4490012f44f2ff1bf0c713bb8f3145fce0e3ef669c7c15f19a59ac157838951f234c17b81ca655d  pnpm-8.12.1.tgz
"
